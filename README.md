
# xls2jra

## Purpose

The tool `xls2jra` consumes CSV files containing task lists, including descriptions and hierarchy information, that were exported from  Primavera P6. Those tasks are then batch processed and added to the local ICS JIRA installation.


## Execution

Run `python xls2jra.py` on the command line. Python 2.7 is a prerequisite.

## TODO
- add command line parameter for file name
- JIRA integration