
""" xls2jra: Importing ICS tasks into JIRA """

__author__     = "Gregor Ulm"
__copyright__  = "Copyright 2016, European Spallation Source, Lund"
__credits__    = [ "Gregor Ulm"
                 , "Nick Levchenko"
                 ]
__license__    = "GPLv3"
__maintainer__ = "Gregor Ulm"
__email__      = "gregor.ulm@esss.se"
__status__     = "Development"
__env__        = "Python version 2.7"


import copy
import json


"""
TODO:

Current status:

every entry in the sample CSV file is converted into a JSON object.

Example resulting JSON object:
{"fields":
    { "startdate"   : "02/06/16"
    , "enddate"     : "16/09/16"
    , "primaveraid" : "A20156750"
    , "summary"     : "Derive Technical Requirements for WP-11 Interfaces and implement comments from SRR"
    , "project"     : {"key": "IHITPE"}
    , "linkedissue" : "is part of"
    , "issuetype"   : "Story"
    , "issue"       : "14.11"
    }
}


Next steps:
- verify that created JSON arrays are syntactically correct
- REST integration with:
  https://ess-ics.atlassian.net/projects/IHITPE/summary

"""


FILENAME = "sample_input/input2.csv"


def cleanUpLines(lines):
    assert isinstance(lines, list)

    lines  = map(lambda x: x.split(";"), lines)
    result = []

    for line in lines:
        assert len(line) == 5, line
        line = map(lambda x: x.strip(), line)
        line = tuple(line)
        result.append(line)

    return result

def extractHierarchy(lines):
    assert isinstance(lines, list)

    result = []

    for line in lines:
        (_, val, _, _, _) = line
        if not val.startswith("A"):
            result.append(val)

    # verifying that input is correctly ordered
    tmp = copy.deepcopy(result)
    tmp.sort()
    assert tmp == result, "hierarchy incorrectly defined"

    return result

def parent(levels, val):
    assert isinstance(levels, list)
    assert isinstance(val,    str )

    assert val in levels
    assert val.count(".") > 1, "illegal input"

    while True:
        val = chopOff(val)
        if val in levels:
            return val

    return ""

# removes last part in a hierarchy name
# e.g. "foo.bar.baz" -> "foo.bar"
def chopOff(s):
    assert isinstance(s, str)
    assert "." in s

    pos = None

    for i in range(len(s)):
        if s[i] == ".":
            pos = i

    return s[:pos]

def extractData(lines):
    assert isinstance(lines, list)

    # populate dictionary with all tasks
    # if hierarchy ID is encountered, subsume all subsequent tasks until
    # next hierarchy ID is found
    hierarchy_vals = []
    tasks          = dict()

    currentLevel = None
    for (_, ident, text, start, end) in lines:
        assert ident.startswith("14") or ident.startswith("A"), ident

        entry = (ident, text, start, end)

        if ident.startswith("14"):
            currentLevel = ident
            hierarchy_vals.append(entry)

        elif ident.startswith("A"):
            if currentLevel in tasks.keys():
                tasks[currentLevel].append(entry)
            else:
                tasks[currentLevel] = [entry]

    return (hierarchy_vals, tasks)


def JIRA_Entry(parentIssue, issueType, elem):
    assert isinstance(parentIssue, str  )
    assert isinstance(issueType,   str  )
    assert isinstance(elem,        tuple)

    # ('A20156760', 'Wheel control system preliminary technical design', '19/09/16', '25/11/16')
    (primaveraID, summary, startDate, endDate) = elem

    if issueType == "Epic":
        parentIssue = None   # TODO: can a top-level element be its own parent?
                             # Concretely, what is the JIRA entry for 14.11 supposed
                             # to look like?

    fieldsDict = dict()
    fieldsDict["project"]     = { "key": "IHITPE" }
    fieldsDict["primaveraid"] = primaveraID
    fieldsDict["issuetype"  ] = issueType
    fieldsDict["summary"    ] = summary
    fieldsDict["startdate"  ] = startDate
    fieldsDict["enddate"    ] = endDate
    fieldsDict["linkedissue"] = "is part of"
    fieldsDict["issue"      ] = parentIssue

    return { "fields": fieldsDict }


def updateJIRA(entry):
    assert isinstance(entry, dict)

    # convert Python dict to JSON object for JIRA
    jsonarray = json.dumps(entry)

    # TODO
    # REST request (POST)


def printEntry(entry):

    fields = entry["fields"]
    for field in fields:
        print field, fields[field]
    print "\n"

if __name__ == "__main__":

    # read file
    with open(FILENAME) as f:
        lines = f.readlines()

    # drop first line (headers)
    lines = lines[1:]

    # turn into tuples
    lines = cleanUpLines(lines)

    # get all hierarchy designators
    levels = extractHierarchy(lines)

    # create intermediate data structures containing required values
    (hierarchy_vals, tasks) = extractData(lines)


    # uncomment these lines to print intermediate data
    """
    print "all hierarchy values:"
    for x in hierarchy_vals:
        print x

    print "+" * 60

    for x in tasks.keys():
        print "Task:"
        print x, ", parent: ", parent(levels, x)

        for y in tasks[x]:
            print y
        print "\n"

    """


    # iterate through hierarchy values:
    for (h, summary, start, end) in hierarchy_vals:

        print "=" * 60
        print "Processing:", h

        # TODO: top-level entry is semantically different; discuss with Nick what the
        # corresponding JIRA entry should look like
        # simple solution: make 'null' the parent of "14.11"
        if h == "14.11":
            x = JIRA_Entry(h, "Epic", (None, summary, start, end) )
            printEntry(x)

            updateJIRA(x)

        else:
            associated_tasks = tasks[h]
            for elem in associated_tasks:
                # elem, eg.
                # ('A20156760', 'Wheel control system preliminary technical design', '19/09/16', '25/11/16')

                parentIssue = parent(levels, h)
                # at this point we have all values we need for creating a JIRA entry
                x = JIRA_Entry(parentIssue, "Story", elem)
                printEntry(x)

                updateJIRA(x)
